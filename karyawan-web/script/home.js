function showData() {
    const socket = new WebSocket("ws://localhost:8080/getKaryawan");
    let jsonRes = "";
    const tbody = document.getElementsByTagName("tbody")[0];
    const totalKaryawan = document.getElementById("totalKaryawan");
    
    socket.onopen = function (event) {
        console.log(event.data)
        socket.send("Hallo Server!")
     };

    socket.onmessage = function (event) {
        let no = 1;
        jsonRes = event.data;
        const obj = JSON.parse(jsonRes);
         obj.data.forEach(data => {
            if (data.nik) {
                tbody.innerHTML += `
                <tr>
                    <th scope="row">${no}</th>
                    <td>${data.nik}</td>
                    <td>${data.nama}</td>
                    <td>${data.tempat_tgl}</td>
                    <td>${data.jenis_kelamin}</td>
                    <td>${data.alamat}</td>
                    <td>${data.rt_rw}</td>
                    <td>${data.kel_desa}</td>
                    <td>${data.kecamatan}</td>
                    <td>${data.agama}</td>
                    <td>${data.status}</td>
                    <td>${data.pekerjaan}</td>
                    <td>${data.kewarganegaraan}</td>
                </tr>`;
                totalKaryawan.innerHTML = `Total Karyawan: ${no}`;
                no++;
            }
        });
    };
 }

showData();