function insertData() {
    const socket = new WebSocket("ws://localhost:8080/karyawan");
    let jsonReq = "";
    
    const nik = document.getElementById("nikInput").value;
    const nama = document.getElementById("namaInput").value;
    const tempatLahir = document.getElementById("tempatInput").value;
    const jenisKelamin = document.getElementById("jenisKelaminSelect").value;
    const alamat = document.getElementById("alamatInput").value;
    const rtRw = document.getElementById("rtRwInput").value;
    const kelDesa = document.getElementById("kelDesaInput").value;
    const kecamatan = document.getElementById("kecInput").value;
    const agama = document.getElementById("agamaSelect").value;
    const status = document.getElementById("statusSelect").value;
    const pekerjaan = document.getElementById("pekerjaanInput").value;
    const kewarganegaraan = document.getElementById("wargaInput").value;

    
    if (jenisKelamin != "Pilih.." && agama != "Pilih.." && status != "Pilih..") {
        jsonReq = JSON.stringify({
            nik: nik,
            nama: nama,
            tempat_tgl: tempatLahir,
            jenis_kelamin: jenisKelamin,
            alamat: alamat,
            rt_rw: rtRw,
            kel_desa: kelDesa,
            kecamatan: kecamatan,
            agama: agama,
            status: status,
            pekerjaan: pekerjaan,
            kewarganegaraan: kewarganegaraan
        })

        socket.onopen = function (event) {
            socket.send(jsonReq);
            alert("Data Berhasil Disimpan");
            window.location.href = 'http://127.0.0.1:5501/index.html';
        };
        
        socket.onmessage = function (event) {
            console.log(event.data);
        };
    } else {
        alert("Data yang dimasukan salah");
    }  
}