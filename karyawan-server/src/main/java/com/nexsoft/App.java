package com.nexsoft;


import com.nexsoft.websocket.ServletGetKaryawan;
import com.nexsoft.websocket.ServletKaryawan;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.ServerConnector;
import org.eclipse.jetty.servlet.ServletContextHandler;

public class App 
{
    public static void main( String[] args )
    {
        Server server = new Server();
        ServerConnector connector = new ServerConnector(server);
        connector.setPort(8080);
        server.addConnector(connector);

        ServletContextHandler handler = new ServletContextHandler(server, "/");
        handler.addServlet(ServletKaryawan.class, "/karyawan");
        handler.addServlet(ServletGetKaryawan.class, "/getKaryawan");

        try {
            server.start();
            server.join();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
