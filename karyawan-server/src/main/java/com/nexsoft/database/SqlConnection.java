package com.nexsoft.database;

import org.json.JSONObject;

import java.sql.*;

public class SqlConnection {
    public void insertData(String data) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/karyawan?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();

            JSONObject object = new JSONObject(data);

            if (object.length() != 0) {
                String query = "INSERT INTO data " + " VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
                PreparedStatement preparedStatement = myConn.prepareStatement(query);
                preparedStatement.setInt(1, object.getInt("nik"));
                preparedStatement.setString(2, object.getString("nama"));
                preparedStatement.setString(3, object.getString("tempat_tgl"));
                preparedStatement.setString(4, object.getString("jenis_kelamin"));
                preparedStatement.setString(5, object.getString("alamat"));
                preparedStatement.setString(6, object.getString("rt_rw"));
                preparedStatement.setString(7, object.getString("kel_desa"));
                preparedStatement.setString(8, object.getString("kecamatan"));
                preparedStatement.setString(9, object.getString("agama"));
                preparedStatement.setString(10, object.getString("status"));
                preparedStatement.setString(11, object.getString("pekerjaan"));
                preparedStatement.setString(12, object.getString("kewarganegaraan"));
                preparedStatement.execute();
            }
            myConn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
     }


    public String getData() {
        String res = "{\"data\": [";
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");

            Connection myConn = DriverManager.getConnection("jdbc:mysql://localhost:3306/karyawan?useUnicode" +
                    "=true&useJDBCCompliantTimeZoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC", "root", "");
            Statement statement = myConn.createStatement();
            String query = "SELECT * FROM data";

            ResultSet rs = statement.executeQuery(query);
            while (rs.next()) {
                res += "{" +
                        "\"nik\": " + rs.getInt("nik") + "," +
                        "\"nama\": \"" + rs.getString("nama") + "\"," +
                        "\"tempat_tgl\": \"" + rs.getString( "tempat_tgl") + "\"," +
                        "\"jenis_kelamin\": \"" + rs.getString("jenis_kelamin") + "\"," +
                        "\"alamat\": \"" + rs.getString("alamat") + "\"," +
                        "\"rt_rw\": \"" + rs.getString("rt_rw") + "\"," +
                        "\"kel_desa\": \"" + rs.getString("kel_desa") + "\"," +
                        "\"kecamatan\": \"" + rs.getString("kecamatan") + "\"," +
                        "\"agama\": \"" + rs.getString("agama") + "\"," +
                        "\"status\": \"" + rs.getString("status") + "\"," +
                        "\"pekerjaan\": \"" + rs.getString("pekerjaan") + "\"," +
                        "\"kewarganegaraan\": \"" + rs.getString("kewarganegaraan") + "\"" +
                        "},";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        res += "{}]}";
        return res;
    }

    public int getCountData() {
        int row = 0;
        return row;
    }
}
