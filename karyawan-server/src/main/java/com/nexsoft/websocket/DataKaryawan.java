package com.nexsoft.websocket;

import com.nexsoft.database.SqlConnection;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.*;

import java.io.IOException;

@WebSocket
public class DataKaryawan {
    private Session session;
    SqlConnection sqlConnection = new SqlConnection();

    @OnWebSocketClose
    public void onClose(int statusCode, String reason) {
        System.out.println("Close: statusCode=" + statusCode + ", reason=" + reason);
    }

    @OnWebSocketError
    public void onError(Throwable t) {
        System.out.println("Error: " + t.getMessage());
    }

    @OnWebSocketConnect
    public void onConnect(Session session) {
        this.session = session;
        System.out.println("Connect: " + session.getRemoteAddress().getAddress());
        try {
            session.getRemote().sendString("Hello Client!");
        } catch (IOException | NullPointerException e) {
            System.out.println(e.getMessage());
        }
    }

    @OnWebSocketMessage
    public void onMessage(String message) throws NullPointerException {
        try {
            if (message != "") {
                sqlConnection.insertData(message);
                this.session.close();
            }
            System.out.println(message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
